const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('./models/mongodb/users');

passport.use(
  new LocalStrategy(
    {
      usernameField: 'username',
      passwordField: 'password',
    },
    async (username, password, done) => {
      try {
        const user = await User.findOne({
          username,
        });
        if (!user) {
          return done(null, false, { message: 'Email or Password invalid.' });
        }
        const match = await user.validPassword(password);
        if (!match) {
          return done(null, false, { message: 'Email or Password invalid.' });
        }
        return done(null, user);
      } catch (error) {
        return done(null, false, { message: error });
      }
    }
  )
);
