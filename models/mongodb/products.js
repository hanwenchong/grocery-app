const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  brand: {
    type: String,
    required: true,
  },
  barcode: Number,
});

ProductSchema.methods.validator = function() {
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    brand: Joi.string().required(),
  });
  return schema.validate(this, {
    abortEarly: false,
    allowUnknown: true,
  });
};

module.exports = mongoose.model('products', ProductSchema);
