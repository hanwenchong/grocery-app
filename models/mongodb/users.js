const Joi = require('@hapi/joi');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { SECRET } = require('../../config');

const UserSchema = new mongoose.Schema({
  username: String,
  password: String,
  role: {
    type: String,
    enum: ['SUPER_ADMIN', 'USER'],
    default: 'USER',
  },
});

UserSchema.methods.validator = function() {
  const schema = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.string().required(),
    role: Joi.string()
      .valid('SUPER_SUPER_ADMIN', 'SUPER_ADMIN', 'ADMIN', 'USER')
      .default('USER'),
  });
  return schema.validate(this, {
    abortEarly: false,
    allowUnknown: true,
  });
};

UserSchema.methods.setPassword = async function(password) {
  const hash = await bcrypt.hash(password, 10);
  this.password = hash;
};

UserSchema.methods.validPassword = async function(password) {
  const match = await bcrypt.compare(password, this.password);
  return match;
};

UserSchema.methods.generateJWT = function() {
  return jwt.sign(
    {
      id: this.id,
    },
    SECRET,
    {
      header: {
        typ: 'access',
      },
      audience: 'exyte',
      issuer: 'https://conny.tech',
      algorithm: 'HS256',
      expiresIn: '1w',
    }
  );
};

UserSchema.methods.toProfileJSON = function() {
  return {
    id: this.id,
    username: this.username,
    role: this.role,
    token: this.generateJWT(),
  };
};

module.exports = mongoose.model('users', UserSchema);
