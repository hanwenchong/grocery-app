const mongoose = require('mongoose');
const { DATABASE } = require('./config');

const { MONGO } = DATABASE;

module.exports = () => {
  // MongoDB database connectionString
  const connectionString = `mongodb+srv://${MONGO.USERNAME}:${MONGO.PASSWORD}@${MONGO.HOST}`;

  mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });

  const db = mongoose.connection;
  db.once('open', () => console.log('connected to the database'));

  db.on('error', err => console.log(`Mongoose default connection has occured ${err} error`));
  db.on('disconnected', () => console.log('Mongoose default connection is disconnected'));

  process.on('SIGINT', () =>
    db.close(() => {
      console.log('Mongoose default connection is disconnected due to application termination');
      process.exit(0);
    })
  );
};
