require('dotenv').config();
const config = require('config');

const APP = config.get('APP');
const API = config.get('API');
const DATABASE = config.get('DATABASE');

const { APP_PORT, NODE_ENV } = process.env;

const IS_PROD = NODE_ENV === 'production';

const sharedConfig = {
  APP_PORT: APP.PORT || APP_PORT,
  NODE_ENV,
  API_URL: API.URL,
  IS_PROD,
  API_VERSION: API.VERSION,
  DATABASE,
  SECRET: APP.SECRET,
};

module.exports = sharedConfig;
