const express = require('express');
const Joi = require('@hapi/joi');
const Product = require('../../../models/mongodb/products');
const auth = require('../../auth');

const { paginate, createSearch } = require('../../../utils/nosql.util');
const { removeEmptyKeyFrom, removeDuplicateKeys } = require('../../../utils/db.util');

const router = express.Router();

/**
 * A function to create product.
 *
 * @returns {number} status code
 */
router.post('/products', auth.required, async (req, res, next) => {
  try {
    const { body } = req;
    if (!body) {
      return res.status(400).json({ message: 'Please provides correct data.' });
    }
    const product = new Product();
    product.name = body.name || null;
    product.brand = body.brand || null;
    product.barcode = body.barcode || null;

    const { error } = product.validator();
    if (error) {
      return res.status(400).json({ messages: error.details });
    }

    await product.save();
    return res.status(201).json({ product });
  } catch (err) {
    return next(err);
  }
});

/**
 * A function to update bulk user information.
 *
 * @returns {object} contains {number} status code and {object} user.
 */
router.patch('/products/:id', async (req, res, next) => {
  try {
    const patchSchema = Joi.object({
      name: Joi.string().allow(null),
      brand: Joi.string().allow(null),
      barcode: Joi.number().allow(null),
    });

    const { error } = patchSchema.validate(req.body, {
      abortEarly: false,
      allowUnknown: true,
    });
    if (error) {
      return res.status(400).json({ messages: error.details });
    }

    const result = await Product.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      {
        new: true,
      }
    );
    if (!result) {
      return res.status(404).json({ message: 'Product not found.' });
    }
    return res.status(200).json(result);
  } catch (err) {
    return next(err);
  }
});

/**
 * A function to get products.
 *
 * @returns {object} contains status code and array of object
 */
router.get('/products', async (req, res, next) => {
  try {
    const paramsSchema = Joi.object({
      current: Joi.number()
        .integer()
        .allow(null),
      pageSize: Joi.number()
        .integer()
        .allow(null),
      sortOrder: Joi.string()
        .allow('ascend', 'descend')
        .default('ascend'),
      sortField: Joi.string().default('username'),
      filters: Joi.object().allow(null),
      search: Joi.object().allow(null),
      join: Joi.object().allow(null),
    });
    const { error } = paramsSchema.validate(req.query, {
      abortEarly: false,
    });
    if (error) {
      return res.status(400).json({ messages: error.details });
    }
    const {
      current = 0,
      pageSize = 10,
      sortOrder = 'ascend',
      sortField = 'name',
      search = {},
      filters = {},
    } = req.query;
    const sortOrderCfg = sortOrder === 'ascend' ? 1 : -1;
    const cleanedSearch = removeEmptyKeyFrom(search);
    const pagination = paginate({
      current: parseInt(current, 10),
      pageSize: parseInt(pageSize, 10),
    });
    // mongoose only support call by twice api to get total count and data.
    const queries = {
      ...createSearch({ params: cleanedSearch, sql: false }),
      ...filters,
    };
    const total = await Product.countDocuments({ ...queries });
    const data = await Product.find({ ...queries }, null, {
      sort: { [sortField]: sortOrderCfg },
      ...pagination,
    });
    return res.status(200).json({ products: data, total });
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
