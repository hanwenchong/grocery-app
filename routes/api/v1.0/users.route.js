const express = require('express');
const passport = require('passport');
const Joi = require('@hapi/joi');
const User = require('../../../models/mongodb/users');
const Product = require('../../../models/mongodb/products');
const auth = require('../../auth');

const router = express.Router();

/**
 * A function to get requester's information and re-generated token.
 *
 * @returns {object} contains {number} status code and {object} user.
 */
router.get('/user', auth.required, async (req, res, next) => {
  try {
    const user = await User.findById(req.payload.id);
    const requester = user.toProfileJSON();

    return res.status(200).json({ user: requester });
  } catch (err) {
    return next(err);
  }
});

/**
 * A function to create user.
 *
 * @returns {number} status code
 */
router.post('/users', async (req, res, next) => {
  try {
    const { body } = req;
    if (!body) {
      return res.status(400).json({ message: 'Please provides correct data.' });
    }
    const user = new User();
    user.username = body.username || null;
    user.password = body.password || null;
    user.role = body.role || 'USER';

    const { error } = user.validator();
    if (error) {
      return res.status(400).json({ messages: error.details });
    }
    await user.setPassword(body.password);
    await user.save();
    return res.status(201).json({ user });
  } catch (err) {
    return next(err);
  }
});
/**
 * A function to login user.
 *
 * @returns {object} user infomation and jwt token.
 */
router.post('/users/login', (req, res, next) => {
  try {
    const loginSchema = Joi.object({
      username: Joi.string().required(),
      password: Joi.string().required(),
    }).with('username', 'password');
    const { error } = loginSchema.validate(req.body, {
      abortEarly: false,
    });
    if (error) {
      return res.status(400).json({ messages: error.details });
    }
    passport.authenticate('local', { session: false }, async (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (user) {
        const userJSON = user.toProfileJSON();
        return res.status(200).json({ user: userJSON });
      }
      console.log(info);
      return res.status(400).json(info);
    })(req, res, next);
  } catch (err) {
    return next(err);
  }
  return null;
});

/**
 * A function to delete products.
 *
 * @returns {number} status code
 */
router.delete('/products/:id', auth.required, async (req, res, next) => {
  try {
    const product = await Product.findByIdAndDelete(req.params.id);
    if (!product) {
      return res.sendStatus(404);
    }
    return res.sendStatus(200);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
