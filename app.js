const compression = require('compression');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const errorhandler = require('errorhandler');
const helmet = require('helmet');
const useragent = require('express-useragent');
const mongoose = require('mongoose');

const { IS_PROD } = require('./config');
const api = require('./routes');

const app = express();
app.use(helmet());
app.use(compression());
app.use(cors());
app.use(useragent.express());

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if (!IS_PROD) {
  app.use(errorhandler());
  mongoose.set('debug', true);
}

require('./passport');

// append /api/{version} for our http requests
app.use(api);

if (!IS_PROD) {
  app.use((err, req, res, next) => {
    console.log(err.stack);

    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

module.exports = app;
