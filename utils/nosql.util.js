exports.paginate = ({ current = 0, pageSize = 10 }) => {
  const skip = current * pageSize;
  const limit = pageSize;

  return {
    skip,
    limit,
  };
};

exports.createSearch = ({ Op, params = {}, sql = true }) => {
  const filters = {};

  Object.keys(params).map(k => {
    filters[k] = new RegExp(params[k], 'i');
    return filters;
  });
  return Object.keys(filters).length !== 0 ? filters : null;
};
