exports.removeDuplicateKeys = ({ filters, join, search }) => {
  const newObj = {};

  Object.keys(search).forEach(key => {
    if (!(key in filters) && !(key in join)) {
      newObj[key] = search[key];
    }
  });

  return newObj;
};

const removeEmptyKeyFrom = obj => {
  const newObj = {};

  Object.keys(obj).forEach(key => {
    if (obj[key] && typeof obj[key] === 'object') {
      newObj[key] = removeEmptyKeyFrom(obj[key]);
    } else if (obj[key] != null) {
      newObj[key] = obj[key];
    }
  });

  return newObj;
};

exports.removeEmptyKeyFrom = removeEmptyKeyFrom;
